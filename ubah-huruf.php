<?php
function ubah_huruf($string){
    $letter = "abcdefghijklmnopqrstuvwxyz";
    $bucket = "";
    for ($i = 0; $i < strlen($string); $i++){
        $pos = strrpos($letter, $string[$i]);
        $bucket .= substr($letter, $pos + 1, 1);
    }
    return $bucket;
}

// TEST CASES
echo ubah_huruf('wow'); echo "<br>"; // xpx
echo ubah_huruf('developer'); echo "<br>"; // efwfmpqfs
echo ubah_huruf('laravel'); echo "<br>"; // mbsbwfm
echo ubah_huruf('keren'); echo "<br>"; // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>